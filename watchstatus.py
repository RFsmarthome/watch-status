import hassapi as hass
import adbase as ad
from datetime import datetime, timedelta, time
from dateutil import parser
from pytz import timezone

class WatchStatus(ad.ADBase):

    def initialize(self):
        self.hass = self.get_plugin_api("HASS")
        self.ad = self.get_ad_api()

        time = datetime.now()
        self.ad.run_minutely(self.check_sensors, time)

        self.sensors = self.args["sensors"]
        self.timezone = timezone(self.args["time_zone"])

        self.check_sensors(None)

    def check_sensors(self, kwargs):
        now = datetime.now()
        now = self.timezone.localize(now)
        
        for sensor in self.sensors:
            try:
                self.ad.log("Check sensor: %s (%d)" % (sensor["name"], sensor["time"]), level="INFO")

                statusEntity = self.hass.get_entity(sensor["status"])

                entity = self.hass.get_entity(sensor["name"])
                state = entity.get_state(attribute="all")
                lastUpdated = parser.parse(state["last_updated"])

                statusEntity.set_state(state="off")
                if now-lastUpdated > timedelta(seconds=sensor["time"]):
                    self.ad.log("Alarm for sensor %s" % sensor["name"])
                    statusEntity.set_state(state="on")
            except:
                self.ad.log("Error in sensor, try next", level="ERROR")
